# detrzip

A cli wrapper around [bitgenics/deterministic-zip](https://github.com/bitgenics/deterministic-zip). Produces determinstic zip files based on content.

## Usage

```bash
❯ npx detrzip --help
detrzip.js <zipfile> <directory>

create a deterministic zip

Positionals:
  zipfile    the name of the zipfile to create                          [string]
  directory  directory to include in the archive                        [string]

Options:
  --version   Show version number                                      [boolean]
  --help      Show help                                                [boolean]
  --includes  list of file patterns to include in archive
                                                     [array] [default: ["./**"]]
  --excludes  list of file patterns to exclude from archive
  [array] [default: [".git","CVS",".svn",".hg",".lock-wscript",".wafpickle-N","*
                                       .swp",".DS_Store","._*","npm-debug.log"]]
```