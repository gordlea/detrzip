import pkgJson from './package.json';
import babel from '@rollup/plugin-babel';
import commonjs from '@rollup/plugin-commonjs';
import resolve from '@rollup/plugin-node-resolve';
import shebang from 'rollup-plugin-add-shebang';
import { terser } from 'rollup-plugin-terser';
import clear from 'rollup-plugin-clear';

const internalDeps = [
];

const externalDeps = [
    ...Object.keys(pkgJson.dependencies || {}).filter((d) => internalDeps.indexOf(d) === -1),
    ...Object.keys(pkgJson.peerDependencies || {}),
];


const config = {
    input: 'src/detrzip.js',
    output: [{
        file: 'dist/detrzip.js',
        format: 'cjs',
        sourcemap: true,
    }],
    plugins: [
        commonjs(),
        resolve({
            preferBuiltins: true,
        }),
        babel({ babelHelpers: 'bundled' }),
        shebang({
            include: 'dist/detrzip.js',
        }),
    ],
    external(id) {
        if (externalDeps.indexOf(id) !== -1) {
            return true;
        }
        return false;
    },
};

if (process.env.CLEAN_BUILD === 'true') {
    config.plugins.unshift(clear({ targets: ['dist'] }));
}

if (process.env.NODE_ENV === 'production') {
    config.plugins.push(terser());
    config.output = config.output.map((output) => {
        return {
            ...output,
            compact: true,
        }
    })
}

export default config;