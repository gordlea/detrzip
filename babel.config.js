module.exports = {
    presets: [
        [
            '@babel/preset-env',
            {
                modules: false,
                useBuiltIns: 'usage',
                loose: true,
                targets: 'maintained node versions',
                corejs: 3,
            },
        ],
    ],
};