import zip from 'deterministic-zip';
import yargs from 'yargs';

const argv = yargs
    .command('* <zipfile> <directory>', 'create a deterministic zip', (yargs) => {
        yargs.positional('zipfile', {
            describe: 'the name of the zipfile to create',
            demandOptions: true,
            type: 'string',
        })
        .positional('directory', {
            describe: 'directory to include in the archive',
            demandOptions: true,
            type: 'string',
        })
        .option('includes', {
            description: 'list of file patterns to include in archive',
            type: 'array',
            default: ['./**'],
        })
        .option('excludes', {
            description: 'list of file patterns to exclude from archive',
            type: 'array',
            default: ['.git', 'CVS', '.svn', '.hg', '.lock-wscript', '.wafpickle-N', '*.swp', '.DS_Store', '._*', 'npm-debug.log'],
        })
    }, (argv) => {
        zip(argv.directory, argv.zipfile, {includes: argv.includes, excludes: argv.excludes, cwd: argv.directory}, (err) => {
            if (err) {
                console.error(err);
                process.exit(1);
            }
        });
    })
    .help()
    .argv


